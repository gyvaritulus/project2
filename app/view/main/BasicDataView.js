Ext.define('Project2.view.dataview.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'detail',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Project2.store.DetailPersonnel'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },
    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        id: 'mydataview',
        cls: 'dataview-basic',
        itemTpl: 'Name: {name}<br>Email: {email}<br>Phone: {phone}<br><button onclick="onUpdatePersonnel({user_id})" class="btn btn-primary"> Edit </button><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button><hr>',
        bind: '{personnel}',
       /* plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Name: </td><td>{name}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' 
        }*/
    }]
});