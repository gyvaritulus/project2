Ext.define('Project2.view.main.FormPanel', {
    extend: 'Ext.form.Panel',

    shadow: true,
    xtype: 'editform',
    id: 'editform',
    cls: 'demo-solid-background',
    id: 'basicform',
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    id: 'myname',
                    label: 'Name',
                    placeHolder: 'Your name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    id: 'myemail',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'phone',
                    id: 'myphone',
                    label: 'Phone',
                    placeHolder: '0895618973446',
                    clearIcon: true
                },
                {
                    xtype: 'button',
                    ui: 'action',
                    text: 'Simpan',
                    handler: 'onSimpan'
                },
                  {
                    xtype: 'button',
                    ui: 'confirm',
                    text: 'Tambah personnel',
                    handler: 'onTambah'
                }
             ]   

 });            