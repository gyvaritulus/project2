    Ext.define('Project2.store.Bar', {
        extend: 'Ext.data.Store',
        alias: 'store.bar',
       autoLoad: true,
       

        fields: ['barID', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'name'],

 proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/Project2_php/readBar.php",
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
       