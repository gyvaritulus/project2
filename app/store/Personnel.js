Ext.define('Project2.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId: 'personnel',
    autoLoad: true,
    autoSync: true,

    fields: [
        'name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/Project2_php/personnel.php",
            update: "http://localhost/Project2_php/updatepersonnel.php",
            destroy: "http://localhost/Project2_php/destroypersonnel.php",
            create: "http://localhost/Project2_php/createpersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
