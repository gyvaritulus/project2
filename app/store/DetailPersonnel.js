Ext.define('Project2.store.DetailPersonnel', {
    extend: 'Ext.data.Store',

    alias: 'store.detailpersonnel',
    storeId: 'detailpersonnel',
    //autoLoad: true,
    //autoSync: true,

    fields: [
        'name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/Project2_php/readDetailPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
